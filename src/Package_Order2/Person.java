package Package_Order2;

public class Person {

    String name;
    int age;
    String address;
    
    public Person(String name) {
        this.name = name;
    }
    
    public String toString(){
        return "Buyer's name: " + name;
    }
}
