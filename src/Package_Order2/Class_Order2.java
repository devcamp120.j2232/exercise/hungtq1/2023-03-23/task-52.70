package Package_Order2;

import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.text.NumberFormat;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

public class Class_Order2 {
    
    int id;
    String customerName;
    Integer price;
    Date orderDate;
    Boolean confirm;
    String[] items;
    Person buyer;
    
        
    public Class_Order2(int id, String customerName, Integer price, Date orderDate, boolean confirm, String[] items,
            Person buyer) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = orderDate;
        this.confirm = confirm;
        this.items = items;
        this.buyer = buyer;
    }

    public Class_Order2(int id, String customerName, Integer price) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
    }

    public Class_Order2(String customerName) {
        this.customerName = customerName;
    }

    public Class_Order2() {
    }

    
    public String toString(){
        Locale.setDefault(new Locale("vi", "VN"));
        String pattern = "dd-MMMM-yyyy HH:mm:ss.SSS";
        //DateTimeFormatter defautTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        LocalDateTime now = LocalDateTime.now();
        Locale usLocale = Locale.getDefault();
        //NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);

        return "Order2_Details --- id: " 
        + id
        + " , customerName: " + customerName
        + " , price: " + price
        + " , orderDate: " + now
        + " , confirm: " + confirm
        + " , item: " + Arrays.toString(items)
        + " , buyer: " + buyer;
    }
}
