import java.util.ArrayList;
import java.util.Date;
import Package_Order2.Class_Order2;
import Package_Order2.Person;

public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");
    
        Class_Order2 order1 = new Class_Order2();
        Class_Order2 order2 = new Class_Order2("Hung xu");
        Class_Order2 order3 = new Class_Order2(2, "Ngo", 99999);
        Class_Order2 order4 = new Class_Order2(1, "Tue", 999999, new Date(), true, new String[]{"tim", "hong", "cam"} , new Person("Little duck"));
        
        ArrayList<Class_Order2> arrayOrder = new ArrayList<>();
            arrayOrder.add(order1);
            arrayOrder.add(order2);
            arrayOrder.add(order3);
            arrayOrder.add(order4);

        for (Class_Order2 orderPnt: arrayOrder){
            System.out.println(orderPnt.toString());
        }
    }
}
